%global _empty_manifest_terminate_build 0
Name:           python-pifpaf
Version:        3.2.3
Release:        1
Summary:        Suite of tools and fixtures to manage daemons for testing
License:        Apache-2.0
URL:            https://github.com/jd/pifpaf
Source0:        https://files.pythonhosted.org/packages/90/cf/9409d3fbfb6c7c8df434bcf342bfd5f03497ec50d38b5ae7245ba4cd3557/pifpaf-3.2.3.tar.gz
BuildArch:      noarch

%description
Pifpaf is a suite of fixtures and a command-line tool that allows to start and stop daemons for a quick throw-away usage. 

%package -n python3-pifpaf
Summary:        Suite of tools and fixtures to manage daemons for testing
Provides:       python-pifpaf
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-daiquiri
BuildRequires:  python3-click
BuildRequires:  python3-jinja2
BuildRequires:  python3-fixtures
BuildRequires:  python3-psutil
BuildRequires:  python3-xattr
BuildRequires:  python3-uWSGI
# Tests running requires
# General requires
Requires:       python3-daiquiri
Requires:       python3-click
Requires:       python3-pbr
Requires:       python3-jinja2
Requires:       python3-fixtures
Requires:       python3-psutil
Requires:       python3-xattr
Requires:       python3-uWSGI
# Tests running requires

%description -n python3-pifpaf
Pifpaf is a suite of fixtures and a command-line tool that allows to start and stop daemons for a quick throw-away usage.

%package help
Summary:        Suite of tools and fixtures to manage daemons for testing
Provides:       python3-pifpaf-doc

%description help
Pifpaf is a suite of fixtures and a command-line tool that allows to start and stop daemons for a quick throw-away usage.

%prep
%autosetup -n pifpaf-3.2.3

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .


%files -n python3-pifpaf -f filelist.lst

%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sat Dec 14 2024 wanggang <wanggang1@kylinos.cn> - 3.2.3-1
- Update version to 3.2.3
- Fix list command names from importlib

* Sat Oct 12 2024 liudy <liudingyao@kylinos.cn> - 3.2.2-1
- Update version to 3.2.2

* Wed Aug 21 2024 guochao <guochao@kylinos.cn> - 3.2.1-1
- Update package to version 3.2.1
- Switch to new version of docker actions
- Redis: Use IP address in sentinel configuration
- Replace call to deprecated method Thread.setDaemon()

* Wed Aug 11 2021 wangxiyuan <wangxiyuan1007@gmail.com> - 3.1.5-2
- Correct xattr requires
* Wed Jul 14 2021 OpenStack_SIG <openstack@openeuler.org> - 3.1.5-1
- Package Spec generate
